#include <stdio.h>
#include <stdlib.h>

typedef enum truefalse
{
    FALSE, TRUE
} BOOLEAN;

typedef enum side
{
    ONE, TWO, THREE, FOUR, FIVE, SIX
} dice_side;

/* change the type here to change the size of each element 
 * of the struct
 */

typedef double type;

struct stuff
{
    type a;
    type b;
    type c;
};

struct bits
{
    int i : 5;
    int j : 3;
};

