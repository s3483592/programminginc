#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    char option;
    char ch;
    ch = '*';
    printf("Please select an option:\n");
    printf("A for Task1 \n");
    printf("B for Task2 \n");
    printf("C for Task3 \n");
    printf("D for Task4 \n");
    printf("E for Task 5 \n");
    scanf("%c", &option);

    switch(option){
        case 'A':
            printf("Task 1 was selected \n");
            printf("\n");
            printf("%c \n", ch);
            printf("%c%c \n", ch, ch);
            printf("%c%c%c \n", ch, ch, ch);
            printf("%c%c%c%c \n", ch, ch, ch, ch);
            printf("%c%c%c%c%c \n", ch, ch, ch, ch, ch);
            printf("%c%c%c%c%c%c \n", ch, ch, ch, ch, ch, ch);
            printf("%c%c%c%c%c%c%c \n", ch, ch, ch, ch, ch, ch, ch);
            printf("%c%c%c%c%c%c%c%c \n", ch, ch, ch, ch, ch, ch, ch, ch);
            printf("%c%c%c%c%c%c%c%c%c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("%c%c%c%c%c%c%c%c%c%c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("\n");
            break;
        case 'B':
            printf("Task 2 was selected \n");
            printf("\n");
            printf("         %c \n", ch);
            printf("        %c%c \n", ch, ch);
            printf("       %c%c%c \n", ch, ch, ch);
            printf("      %c%c%c%c \n", ch, ch, ch, ch);
            printf("     %c%c%c%c%c \n", ch, ch, ch, ch, ch);
            printf("    %c%c%c%c%c%c \n", ch, ch, ch, ch, ch, ch);
            printf("   %c%c%c%c%c%c%c \n", ch, ch, ch, ch, ch, ch, ch);
            printf("  %c%c%c%c%c%c%c%c \n", ch, ch, ch, ch, ch, ch, ch, ch);
            printf(" %c%c%c%c%c%c%c%c%c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("%c%c%c%c%c%c%c%c%c%c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("\n");
        case 'C':
            printf("Task 3 was selected \n");
            printf("          %c          \n", ch);
            printf("         %c%c%c         \n", ch, ch, ch);
            printf("        %c%c%c%c%c        \n", ch, ch, ch, ch, ch);
            printf("       %c%c%c%c%c%c%c       \n", ch, ch, ch, ch, ch, ch, ch);
            printf("      %c%c%c%c%c%c%c%c%c      \n", ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("     %c%c%c%c%c%c%c%c%c%c%c     \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("    %c%c%c%c%c%c%c%c%c%c%c%c%c    \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("   %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c   \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("  %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c  \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf(" %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            break;
        case 'D':
            printf("Task 4 was selected \n");
            printf("          %c          \n", ch);
            printf("         %c%c%c         \n", ch, ch, ch);
            printf("        %c%c%c%c%c        \n", ch, ch, ch, ch, ch);
            printf("       %c%c%c%c%c%c%c       \n", ch, ch, ch, ch, ch, ch, ch);
            printf("      %c%c%c%c%c%c%c%c%c      \n", ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("     %c%c%c%c%c%c%c%c%c%c%c     \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("    %c%c%c%c%c%c%c%c%c%c%c%c%c    \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("   %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c   \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("  %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c  \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf(" %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("  %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c%c  \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("   %c%c%c%c%c%c%c%c%c%c%c%c%c%c%c   \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("    %c%c%c%c%c%c%c%c%c%c%c%c%c    \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("     %c%c%c%c%c%c%c%c%c%c%c     \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("      %c%c%c%c%c%c%c%c%c      \n", ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("       %c%c%c%c%c%c%c       \n", ch, ch, ch, ch, ch, ch, ch);
            printf("        %c%c%c%c%c        \n", ch, ch, ch, ch, ch);
            printf("         %c%c%c         \n", ch, ch, ch);
            printf("          %c          \n", ch);
            break;
        case 'E':
            printf("Task 5 was selected \n");
            printf("%c %c %c %c %c %c %c %c %c %c %c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf(" %c %c %c %c %c %c %c %c %c %c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("%c %c %c %c %c %c %c %c %c %c %c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf(" %c %c %c %c %c %c %c %c %c %c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("%c %c %c %c %c %c %c %c %c %c %c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf(" %c %c %c %c %c %c %c %c %c %c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);                        
            printf("%c %c %c %c %c %c %c %c %c %c %c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf(" %c %c %c %c %c %c %c %c %c %c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf("%c %c %c %c %c %c %c %c %c %c %c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf(" %c %c %c %c %c %c %c %c %c %c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);            
            printf("%c %c %c %c %c %c %c %c %c %c %c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);
            printf(" %c %c %c %c %c %c %c %c %c %c \n", ch, ch, ch, ch, ch, ch, ch, ch, ch, ch);            
            break;
        default:
            printf("Your selection was invalid \n");

    }

    return EXIT_SUCCESS;
}

/** 1.  **/
/** 2.  **/
/** 3.  **/
/** 4.  **/
/** 5.  **/
/** 6.  **/
/** 7.  **/
/** 8.  **/
/** 9.  **/
/** 10. **/
