/** The following program has a range of syntax errors; compile it
    (several times if necessary) to get rid of the syntax errors. 
    After getting rid of syntax errors, eliminate the run-time 
    errors, by examining the result carefully. Run the correct 
    program and note the result, then run it again but with all 
    types changed to double. It should produce a different result. Why is this so? **/


#include <stdio.h>

int main(void)
{

    int miles, yards;
    float kilometres;
    yards = 26;
    miles = 385;
    kilometres = 1.609 * (miles + yards / 1760);
    printf("%d miles %d yards = %f kilometres \n", miles, yards, kilometres);
    return yards; 

}

/** 1. included the correct function start "int main(void)"  **/
/** 2. added all the required ";" that was missing           **/
/** 3. added the missing "#Include <stdio.h>"                **/
/** 4. replaced "%lf" with "%f"                              **/
/** 5. added "return" and a delcared variable                **/

/** OUTPUT: using int and float
    385 miles 26 yards = 619.465027 kilometres               **/
/** OUTPUT using double
    385.000000 miles 26.000000 yards = 619.488769 kilometres **/
/** REASON: The different is how the compiler handles
            int, float, and double types.                    **/

