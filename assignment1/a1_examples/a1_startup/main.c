/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2017 Assignment #1 
* Full Name        : Joshua Daniel Nicholson
* Student Number   : s3483592
* Start up code provided by Paul Miller
***********************************************************************/
#include "main.h"
#include "shared.h"

/**
 * entry point for the system to run your code. Please take the comments below 
 * as hints for the code you need to write in this function.
 **/
int main(void)
{
        /* loop until the user decides to quit */
                /* display the menu */
                /* get the user's selection of option to run */
                /* execute the user's option and if that is to quit, set the 
                 * appropriate variable for exiting or exit directly */
        return EXIT_SUCCESS;
}

