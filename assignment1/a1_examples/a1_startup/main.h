/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2017 Assignment #1 
* Full Name        : Joshua Daniel Nicholson
* Student Number   : s3483592
* Start up code provided by Paul Miller
***********************************************************************/
#include "game.h"
#include <stdlib.h>

/**
 * The purpose of this module is to manage the main menu for the program. Add
 * types and functions that help you to manage the main menu here.
 **/
