#include <stdio.h>
#include <stdlib.h>

char gameBoard[3][3];

void init_gameBoard(void);
void display_board(void);
void read_rest_of_line(void);

/**int main(void)
{
    init_gameBoard();
    display_board();
    return EXIT_SUCCESS;
}**/

void read_rest_of_line(void)
{
        int ch;
        while (ch = getc(stdin), ch != '\n' && ch != EOF)
                ;
        clearerr(stdin);
}

void init_gameBoard(void)
{
    int rows, columns;

    for(rows = 0; rows < 3; rows++)
        for (columns = 0; columns < 3; columns++) gameBoard[rows][columns] = ' ';
}

void display_board(void)
{
    int t;
    int lineCount;
    lineCount = 1;
    printf ("   | 1 | 2 | 3 |");
    printf("\n----------------\n");

    for(t = 0; t < 3; t++)
    {
        printf (" %i | %c | %c | %c |", lineCount, gameBoard[t][0],
        gameBoard[t][1],gameBoard[t][2]);
        lineCount++;
        if(t!=2) printf("\n----------------\n");
    }
    printf("\n----------------\n");
    printf("\n");
}