#include <stdio.h>
#include <stdlib.h>


typedef enum
{
        FALSE,
        TRUE
} BOOLEAN;

void menuStart(void)
{
        printf("\n");
        printf("Welcome to tictactoe \n");
        printf("- - - - - - - - - - - - \n");
        printf("1) Play Game \n");
        printf("2) Exit \n");
}

void askBoardSize(void)
{
        printf("You can play tictactoe on a 3x3 board or a 5x5 board. \n");
        printf("Do you want to play on a board the width of 3 or 5? ");
}

void exitProgramMessage(void)
{
        printf("Exiting game");
}

void invalidSelectionMessage(void)
{
        printf("Invalid selection made");
}

