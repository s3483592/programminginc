#include <stdio.h>
#include <stdlib.h>
#include "main_menu.h"

int main(void)
{
	BOOLEAN exitMenu = FALSE;
	int menuOption;
	do
	{
		menuStart();
		printf("Please enter your choice: ");
		scanf("%i", &menuOption);
		switch(menuOption)
		{
			case 1:
				askBoardSize();
				break;
			case 2:
				exitProgramMessage();
				exitMenu = TRUE;
				break;
			default:
				invalidSelectionMessage();
		}
	}while(!exitMenu);
	return EXIT_SUCCESS;
}
