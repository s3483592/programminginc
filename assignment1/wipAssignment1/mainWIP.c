/***********************************************************************
* CPT220 - Programming in C
* Study Period 4 2017 Assignment #1 
* Full Name        : Joshua Daniel Nicholson
* Student Number   : s3483592
* Start up code provided by Paul Miller
***********************************************************************/
#include "main.h"
#include "shared.h"
#include "drawBoard.c"

/**
 * entry point for the system to run your code. Please take the comments below 
 * as hints for the code you need to write in this function.
 **/
int main(void)
{
        int menuOption;
        int board_dimension;
        int num_in_row;
        char playerName;
        BOOLEAN exitMenu = FALSE;
        do
        {
                printf("\n");
                printf("Welcome to tictactoe \n");
                printf("- - - - - - - - - - - - \n");
                printf("1) Play Game \n");
                printf("2) Exit \n");
                printf("Please enter your choice: ");
                scanf("%i", &menuOption);
                switch(menuOption)
                {
                        case 1:
                                read_rest_of_line();
                                printf("You can play tictactoe on a 3x3 board or a 5x5 board. \n");
                                printf("Do you want to play on a board the width of 3 or 5? ");
                                scanf("%i", &board_dimension);
                                printf("You selected %i as your board dimension \n", board_dimension);
                                /**scanf("%i", &board_dimension); **/
                                read_rest_of_line();
                                printf("How many pieces in a row are required for a win? ");
                                scanf("%i", &num_in_row);
                                printf("You selected %i as the number of tokens in a row you need to win \n", num_in_row);
                                /**scanf("%i", &num_in_row); **/
                                read_rest_of_line();
                                printf("Please enter the name for the human player: ");
                                scanf("%c", &playerName);
                                printf("Your name is %c \n", playerName);
                                /** scanf("%i", current->name;) **/
                                read_rest_of_line();
                                goto case 3;
                        case 2:
                                printf("\n");
                                printf("Exiting program \n");
                                printf("\n");
                                exitMenu = TRUE;
                                break;
                        case 3:
                                init_gameBoard();
                                display_board();
                                printf("\n");
                                break;

                        default:
                                printf("\n");
                                printf("invalid selection, please try again \n");
                                printf("\n");

                }

        } while (!exitMenu);
        /* loop until the user decides to quit */
                /* display the menu */
                /* get the user's selection of option to run */
                /* execute the user's option and if that is to quit, set the 
                 * appropriate variable for exiting or exit directly */
        return EXIT_SUCCESS;
}

