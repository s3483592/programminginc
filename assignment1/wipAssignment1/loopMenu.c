#include <stdio.h>
#include <stdlib.h>
#include "loopMenu.h"


int main(void)
{
    BOOLEAN exitMenu = FALSE;

        do
        {
                printf("\n");
                printf("Welcome to tictactoe \n");
                printf("- - - - - - - - - -   \n");
                printf("1) Play Game \n");
                printf("2) Exit \n");
                scanf("%i", &menuOption);
                switch(menuOption)
                {
                        case 1:
                                printf("\n");
                                printf("selected Menu item 1 \n");
                                printf("\n");
                                printf("Please type your name \n");
                                printf("\n");
                                break;
                        case 2:
                                printf("\n");
                                printf("Exiting program \n");
                                printf("\n");
                                exitMenu = TRUE;
                                break;
                        default:
                                printf("\n");
                                printf("invalid selection, please try again \n");
                                printf("\n");

                }

        } while (!exitMenu);

        return EXIT_SUCCESS;
}
