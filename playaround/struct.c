#include <stdio.h>
#include <stdlib.h>
#include "headerTest.h"

int main(void)
{
    struct person somePerson1;
    struct person somePerson2;
    set_age();
    scanf("%i", &somePerson1.age);
    set_age();
    scanf("%i", &somePerson2.age);
    printf("This is a test \n");
    printf("somePerson1 is %i years old\n", somePerson1.age);
    printf("somePerson2 is %i years old\n", somePerson2.age);
    return EXIT_SUCCESS;
}